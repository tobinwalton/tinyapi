package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

type MockUserService struct{}

// GetUser returns mock UserData
func (*MockUserService) GetUser(userID int) (*UserData, error) {
	var user UserData
	var userJSON = `{
					  "id": 1,
					  "name": "Fake Name",
					  "username": "FakeUsername",
					  "email": "fake@fake.com",
					  "address": {
					    "street": "Washington St",
					    "suite": "123456",
					    "city": "Faketown",
					    "zipcode": "92503-99999",
					    "geo": {
					      "lat": "-37.3159",
					      "lng": "81.1496"
					    }
					  },
					  "phone": "1-951-867-5309",
					  "website": "fake-personality.com",
					  "company": {
					    "name": "FakeCo, LLC",
					    "catchPhrase": "micro-micro-microservices",
					    "bs": "harness real-time e-markets"
					  }
					}`

	err := json.Unmarshal([]byte(userJSON), &user)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// GetPosts returns mock post response for id 1, and nil []UserPost for any other id.
func (*MockUserService) GetPosts(userID int) ([]UserPost, error) {
	var posts []UserPost
	if userID == 1 {
		var postsJSON = `[
						  {
						    "id": 1,
						    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
						    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
						  },
						  {
						    "id": 2,
						    "title": "qui est esse",
						    "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
						  }
						 ]`

		err := json.Unmarshal([]byte(postsJSON), &posts)
		if err != nil {
			return nil, err
		}
	}

	return posts, nil
}

// TestGetUser verifies that json user output can be unmarshaled into provided type.
func TestGetUser(t *testing.T) {
	var userService MockUserService
	user, err := userService.GetUser(1)
	assert.Equal(t, err, nil)
	assert.Equal(t, user.ID, 1)
	assert.Equal(t, user.Name, "Fake Name")
}

// TestGetPosts verifies that json post output can be unmarshaled into provided type.
func TestGetPosts(t *testing.T) {
	var userService MockUserService
	posts, err := userService.GetPosts(1)
	assert.Equal(t, err, nil)
	assert.Equal(t, posts[0].ID, 1)
}

// TestGetUserPosts verifies that no errors are returned when retrieving a user with posts.
func TestGetUserPosts(t *testing.T) {
	var userService *MockUserService
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/v1/user-posts/:id")
	c.SetParamNames("id")
	c.SetParamValues("1")
	h := &handler{userService}

	if assert.NoError(t, h.getUserPosts(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}

// TestGetUserNoPosts checks that a user without post data returns without errors.
func TestGetUserNoPosts(t *testing.T) {
	var userService *MockUserService
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/v1/user-posts/:id")
	c.SetParamNames("id")
	c.SetParamValues("2")
	h := &handler{userService}

	if assert.NoError(t, h.getUserPosts(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}
