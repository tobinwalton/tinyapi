module tinyApi

go 1.15

require (
	github.com/labstack/echo/v4 v4.1.17
	github.com/stretchr/testify v1.4.0
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a
)
