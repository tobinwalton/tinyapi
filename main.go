package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"golang.org/x/sync/errgroup"
)

type UserData struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	Username string `json:"username"`
	Email    string `json:"email"`
}

type UserPost struct {
	ID    int    `json:"id"`
	Title string `json:"title"`
	Body  string `json:"body"`
}

type UserPostResponse struct {
	ID       int `json:"id"`
	Userinfo struct {
		Name     string `json:"name"`
		Username string `json:"username"`
		Email    string `json:"email"`
	} `json:"userinfo"`
	Posts []UserPost `json:"posts"`
}

type UserService interface {
	GetUser(userId int) (*UserData, error)

	GetPosts(userId int) ([]UserPost, error)
}

// HttpUserService empty struct is method receiver.
type HttpUserService struct{}

// handler type has a UserService to provide GetUser() and GetPosts() methods.
type handler struct {
	UserService
}

func (*HttpUserService) GetUser(userID int) (*UserData, error) {
	var user UserData
	url := fmt.Sprintf("https://jsonplaceholder.typicode.com/users/%d", userID)

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	jsonErr := json.Unmarshal(body, &user)
	if jsonErr != nil {
		return nil, jsonErr
	}

	return &user, nil
}

func (*HttpUserService) GetPosts(userID int) ([]UserPost, error) {
	var posts []UserPost
	url := fmt.Sprintf("https://jsonplaceholder.typicode.com/posts?userId=%d", userID)

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	jsonErr := json.Unmarshal(body, &posts)
	if jsonErr != nil {
		return nil, jsonErr
	}

	return posts, nil
}

func (h *handler) getUserPosts(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	var response UserPostResponse

	// This works but I need to spend time understanding 
	// how to use context idiomatically.
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	g, ctx := errgroup.WithContext(ctx)
	var user *UserData

	g.Go(func() error {
		u, err := h.UserService.GetUser(id)
		if err != nil {
			return err
		}
		user = u
		return nil
	})

	g.Go(func() error {
		p, err := h.UserService.GetPosts(id)
		if err != nil {
			return err
		}
		response.Posts = p
		return nil
	})

	if err := g.Wait(); err != nil {
		return err
	}

	if user.ID == 0 {
		return echo.NewHTTPError(http.StatusNotFound, "user not found")
	}

	response.ID = user.ID
	response.Userinfo.Name = user.Name
	response.Userinfo.Username = user.Username
	response.Userinfo.Email = user.Email

	return c.JSON(http.StatusOK, response)
}

func main() {
	var userService *HttpUserService
	e := echo.New()
	h := &handler{userService}
	e.GET("/v1/user-posts/:id", h.getUserPosts)
	e.Logger.Fatal(e.Start(":8000"))
}
